---
title: Gromit.io API Docs

language_tabs:
  - shell
  - javascript

toc_footers:
  - <a href='https://app.gromit.io/#/register'>Sign Up to create your Key</a>
  - <a href='https://github.com/tripit/slate'>Documentation Powered by Slate</a>

includes:
  - errors

search: true
---

# Welcome to the API doc

You can use the API to discover the the nearest city using an IP or latitude/longitude, user timezone based on the location including day light savings , and what device your client used to enter in your site.  

This product includes:

+ GeoLite data created by <a href="http://www.maxmind.com" target="_blank">MaxMind</a>.
+ TimeZone data created by <a href="https://timezonedb.com" target="_blank">TimezoneDB</a>.
+ Geolocation data created by <a href="http://www.geonames.org" target="_blank">Geonames</a>.

# Get Location, TimeZone and client information
Returns Location and Client information based on the parameters, if neither ip nor latitude and longitude are presents it will use the ip from the source call.

> Example request

```shell
# With shell, you can just pass the correct header with each request
curl https://{your-key}.gromit.io/api
```

```javascript
// Example with jquery
$.getJSON("https://{your-key}.gromit.io/api").then(function(response){
  console.log('latitude: ' + response.location.latitude)
  console.log('longitude: ' + response.location.latitude)
  console.log('browser: ' + response.client.browser)
})
```

> Example response

```json
{
  "client": {
    "userAgent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.106 Safari/537.36",
    "browser": {
      "family": "Chrome",
      "major": "51",
      "minor": "0",
      "patch": "2704"
    },
    "os": {
      "family": "Mac OS X",
      "major": "10",
      "minor": "11",
      "patch": "5"
    },
    "device": {
      "family": "Other"
    }
  },
  "location": {
    "ip": "8.8.8.8",
    "latitude": 37.386,
    "longitude": -122.0838,
    "cityName": "Mountain View",
    "timeZone": {
      "utcOffset": -8,
      "currentOffset": -7,
      "name": "America/Los_Angeles",
      "dtsOffset": -7,
      "changedAt": 1457863200
    },
    "subdivisions": [
      "Santa Clara County",
      "California"
    ],
    "country": {
      "capital": "Washington",
      "currencyCode": "USD",
      "currencyName": "Dollar",
      "language": "en-US",
      "name": "United States",
      "phone": "1",
      "iso": "US"
    },
    "continent": {
      "iso": "NA",
      "name": "North America"
    }
  }
}
```

> Example request with multiple field query

```shell
# Fields are only for location object so it wont be added as attribute 
# in the response
curl https://{your-key}.gromit.io/api?fields=location.country.name,location.country.iso,location.continent.iso,location.cityName
```

```javascript
$.getJSON("https://{your-key}.gromit.io/api?fields=location.country.name,location.country.iso,location.continent.iso,location.cityName")
.then(function(response){
  console.log('response', response)  
})

// or
 
$.getJSON({
  url: 'https://{your-key}.gromit.io/api',
  data: { 
    fields: 'location.country.name,location.country.iso,location.continent.iso,location.cityName' 
  }
}).then(function(response){
   console.log('response', response)
})
```



> Example response with multiple field response

```json
{
	"continent": {
		"iso": "SA"
	},
	"country": {
		"iso": "BR",
		"name": "Brazil"
	},
	"cityName": "Santos"
}
```

> Example request with single field query

```shell
# Only iso attribute from country in locatoion object so the reply will be just text
curl https://{your-key}.gromit.io/api?fields=location.country.iso
```

```javascript
$.get('https://{your-key}.gromit.io/api?fields=location.country.iso')
.then(function(response){
   console.log(response)
})
```

> Example response with single field response (plain text response)

```text
US
```

### HTTP Request

`GET https://{your-key}.gromit.io/api`

<aside class="success">
Remember you must replace <code>{your-key}</code> in the URL with your personal API key.
</aside>

### Query Parameters

Parameter | Description | Example
--------- | ----------- | --------
ip | ip in ipv4 or ipv6 format | 8.8.8.8
latitude | Unit that represent the one of the coordinates at geographic coordinate system. | 36.893685
longitude | Unit that represent the one of the coordinates at geographic coordinate system. | -86.906254
ua | User agent string. | Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36
fields | Comma separated attributes to be returned. | timeZone,city,device

# Bulk requests

> Body request examples

```json
{ 
  "queryItems": [
    {"ip":"8.8.8.8" },
    {"latitude":"37.386","longitude": "-122.0838"},
    {"ip":"98.139.180.149"},
    {"ip":"69.89.31.226"}
  ]
}
```

> You can fetch only some fields:

```json
{
  "queryItems":[
    {"ip":"8.8.8.8" },
    {"latitude":"37.386","longitude": "-122.0838"},
    {"ip":"98.139.180.149"},
    {"ip":"69.89.31.226"}
  ],
  "fields": "location.timeZone,location.cityName,location.country.name"
}
```

> Example of request 

```shell
curl -X POST \
--header "Content-Type:application/json" \
--data '{ "queryItems": [{ "ip":"8.8.8.8" },{ "ip":"4.4.8.8" } ] }' \
https://{{your-key}}.gromit.io/api/bulk
```

```javascript
$.ajax({
  url: 'https://{{your-key}}.gromit.io/api/bulk',
  method: 'POST',
  dataType: 'json',
  contentType: 'application/json',
  data: JSON.stringify({ queryItems: [{ ip:"8.8.8.8" },{ ip:"4.4.8.8" } ] })
}).then(function(response){
  console.log('response', response)
})
```

> Response

```json
[{
  "client": {
    "userAgent": "curl/7.43.0",
    "browser": {
      "family": "Other"
    },
    "os": {
      "family": "Other"
    },
    "device": {
      "family": "Other"
    }
  },
  "location": {
    "ip": "8.8.8.8",
    "latitude": 37.386,
    "longitude": -122.0838,
    "cityName": "Mountain View",
    "timeZone": {
      "utcOffset": -8.0,
      "currentOffset": -7.0,
      "name": "America/Los_Angeles",
      "dtsOffset": -7.0,
      "changedAt": 1457863200
    },
    "subdivisions": ["Santa Clara County", "California"],
    "country": {
      "capital": "Washington",
      "currencyCode": "USD",
      "currencyName": "Dollar",
      "language": "en-US",
      "name": "United States",
      "phone": "1",
      "iso": "US"
    },
    "continent": {
      "iso": "NA",
      "name": "North America"
    }
  }
}, {
  "client": {
    "userAgent": "curl/7.43.0",
    "browser": {
      "family": "Other"
    },
    "os": {
      "family": "Other"
    },
    "device": {
      "family": "Other"
    }
  },
  "location": {
    "ip": "4.4.8.8",
    "latitude": 37.751,
    "longitude": -97.822,
    "cityName": "Cheney",
    "timeZone": {
      "utcOffset": -6.0,
      "currentOffset": -5.0,
      "name": "America/Chicago",
      "dtsOffset": -5.0,
      "changedAt": 1457856000
    },
    "subdivisions": ["Sedgwick County", "Kansas"],
    "country": {
      "capital": "Washington",
      "currencyCode": "USD",
      "currencyName": "Dollar",
      "language": "en-US",
      "name": "United States",
      "phone": "1",
      "iso": "US"
    },
    "continent": {
      "iso": "NA",
      "name": "North America"
    }
  }
}]
```

> Example of request with some fields

```shell
curl -X POST \
--header "Content-Type:application/json" \
--data '{"queryItems": [{ "ip":"8.8.8.8" },{ "ip":"4.4.8.8" } ], "fields": "location.timeZone,location.cityName,location.country.name" }' \
https://{{your-key}}.gromit.io/api/bulk
```

```javascript
$.ajax({
  url: 'https://{{your-key}}.gromit.io/api/bulk',
  method: 'POST',
  dataType: 'json',
  contentType: 'application/json',
  data: JSON.stringify({ 
    queryItems: [{ ip:"8.8.8.8" },{ ip:"4.4.8.8" } ], 
    fields: "location.timeZone,location.cityName,location.country.name" 
  })
}).then(function(response){
  console.log('response', response)
})
```

> Response

```json
[
  {
    "location": {
      "country": {
        "name": "United States"
      },
      "cityName": "Mountain View",
      "timeZone": {
        "utcOffset": -8,
        "currentOffset": -7,
        "name": "America/Los_Angeles",
        "dtsOffset": -7,
        "changedAt": 1457863200
      }
    }
  },
  {
    "location": {
      "country": {
        "name": "United States"
      },
      "cityName": "Cheney",
      "timeZone": {
        "utcOffset": -6,
        "currentOffset": -5,
        "name": "America/Chicago",
        "dtsOffset": -5,
        "changedAt": 1457856000
      }
    }
  }
]
```

### Bulk body request object

Expected a JSON body with:

Parameter | Type | Description | Example
--------- | ---- | ----------- | -------
queryItems | Array of **Query Item objects** (**required**) | A list of filters to find location information | `[ {"ip":"8.8.8.8"},{"latitude":"37.386","longitude": "-122.0838"} ]`
fields | String (Optional) | Comma separated attributes to be returned. | timeZone,city,device

## Endpoint for location and client requests
`POST https://{{your-key}}.gromit.io/api/bulk`

### Query Item Object

Parameter | Description | Example
--------- | ----------- | -------
ip | ip in ipv4 or ipv6 format | 8.8.8.8
latitude | Unit that represent the one of the coordinates at geographic coordinate system. | 36.893685
longitude | Unit that represent the one of the coordinates at geographic coordinate system. | -86.906254
ua | User agent string. | Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36